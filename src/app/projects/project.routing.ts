import { Routes, RouterModule } from '@angular/router';
import { Project } from './project.component';
import { ModuleWithProviders } from '@angular/core';

import { AuthGuard, PreventAuthGuard, ManagerGuard, AdminGuard } from './../_guards/index';

// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
    {
        path: 'login',
        loadChildren: 'app/projects/login/login.module#LoginModule',
        canActivate: [PreventAuthGuard]
    }, {
        path: 'project',
        component: Project,
        children: [
            { path: '', redirectTo: 'search', pathMatch: 'full' },
            {
                path: 'report',
                loadChildren: 'app/projects/report/chart.module#ChartModule',
                canActivate: [ManagerGuard]
            }, {
                path: 'tables',
                loadChildren: 'app/projects/tables/tables.module#TablesModule',
                canActivate: [ManagerGuard]
            }, {
                path: 'search',
                loadChildren: 'app/projects/search/search.module#SearchModule',
                canActivate: [AuthGuard]
            }, {
                path: 'users',
                loadChildren: 'app/projects/users/users.module#UsersModule',
                canActivate: [AdminGuard]
            }, {
                path: 'inputs',
                loadChildren: 'app/projects/inputs/inputs.module#InputsModule',
                canActivate: [AuthGuard]
            }, {
                path: 'updates/:censusCode',
                loadChildren: 'app/projects/inputs/inputs.module#InputsModule',
                canActivate: [AuthGuard]
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
