import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';


import { Chart } from './chart.component';


export const routes: Routes = [
    {
        path: '',
        component: Chart
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);