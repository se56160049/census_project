import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgaModule } from '../../theme/nga.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts/ng2-charts';

import { Chart  } from './chart.component';
import { routing } from './chart.routing';
//chart service
import { ChartistJsService } from './chart.service';
//resolve



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    routing,
    ChartsModule
  ],
  declarations: [
    Chart
  ],
  providers: [
    ChartistJsService

  ]
})

export class ChartModule { }