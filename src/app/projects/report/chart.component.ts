import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl, ControlValueAccessor, } from '@angular/forms';
import { CommonService } from './../../_services/common.service';
import { ChartistJsService } from './chart.service';
import { BaseChartDirective } from 'ng2-charts';

import { GetStatistics } from './chart.interface';

@Component({
  selector: 'chart',
  templateUrl: './chart.html',
  styleUrls: ['./../../theme/components/baContentTop/baContentTop.scss'],
  providers: [CommonService]
})

export class Chart implements OnInit {

  @ViewChild(BaseChartDirective) private _chart;
  //@ViewChild('myModal') modal: ModalComponent;
  public toggle: boolean = false;
  barData: any;

  private allYear = [];
  private presentYear = new Date().getFullYear() + 543;

  private formReport: FormGroup;
  private select_year: AbstractControl;

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{ id: 'y-axis-1', type: 'linear', position: 'left', ticks: { min: 0, max: 3 } }]
    }

  };

  public pieChartOptions: any = {
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var dataset = data.datasets[tooltipItem.datasetIndex];
          var label = data.labels[tooltipItem.index];
          var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
            return previousValue + currentValue;
          });
          var currentValue = dataset.data[tooltipItem.index];
          var percentage = ((currentValue / total) * 100);

          if (Math.floor(percentage) === percentage) {
            return label + ' : ' + currentValue + ' (' + Math.floor(percentage) + '%)';
          } else {
            return label + ' : ' + currentValue + ' (' + percentage.toFixed(2) + '%)';
          }
        }
      }
    }
  }

  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartData: any[] = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];

  // Pie chart option
  public pieChartLabels: string[] = [];
  public pieChartData1: number[] = [];
  public pieChartData2: number[] = [];
  public pieChartType: string = 'pie';
  public pieChartLegend: boolean = false;
  private pieChartColours: any[] = [{ backgroundColor: ['#EC7063', '#8E44AD', '#2E86C1', '#138D75', '#F1C40F', '#2ECC71', '#D35400', '#34495E'] }];

  //constructor
  constructor(private router: ActivatedRoute, private _chartistJsService: ChartistJsService,
    private reportService: CommonService, private fb: FormBuilder, private zone: NgZone) {

    // console.log("constructorreport");
    this.formReport = this.fb.group({

      select_year: [String(this.presentYear).substring(2, 4)]

    });

    this.select_year = this.formReport.controls['select_year'];
  }

  ngOnInit() {
    // console.log("ngOnInitreport");

    this.selectYear();
    this.getChart();

    this.reportService.getDataJson('http://localhost/census_svr/index.php/Inputs/getStatus/')
      .subscribe(
      res => {
        //console.log(res);
        this.barChartLabels = [];
        for (const key of Object.keys(res)) {
          this.barChartLabels.push(res[key].sName);
        }
      });
  }

  public selectYear() {
    this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_year_report/')
      .subscribe(res => {
        this.allYear = res;
      });
  }

  public getChart() {

    let _barChartData: any[] = [
      { data: [], label: '' },
      { data: [], label: '' }
    ];

    this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report/' + this.select_year.value)
      .subscribe(
      res => {

        let i = 0;
        for (const key of Object.keys(res)) {
          _barChartData[i].data = [];
          _barChartData[i].data.push(+(res[key].status1),
            parseInt(res[key].status2),
            parseInt(res[key].status3),
            parseInt(res[key].status4),
            parseInt(res[key].status5),
            parseInt(res[key].status6),
            parseInt(res[key].status7),
            parseInt(res[key].status8));

          _barChartData[i].label = '25' + res[key].year;

          i++;
        }

        this.barChartData = _barChartData;
        this.pieChartData1 = _barChartData[1].data;
        this.pieChartData2 = _barChartData[0].data;
        //calculate max chart
        if(Math.max.apply(null, this.pieChartData1) > 3 || Math.max.apply(null, this.pieChartData2) > 3){
          if (Math.max.apply(null, this.pieChartData1) > Math.max.apply(null, this.pieChartData2)) {
           this.barChartOptions.scales.yAxes[0].ticks.max = Math.round(Math.max.apply(null, this.pieChartData1) + (Math.max.apply(null, this.pieChartData1) / 4));
          } else {
            this.barChartOptions.scales.yAxes[0].ticks.max = Math.round(Math.max.apply(null, this.pieChartData2) + (Math.max.apply(null, this.pieChartData2) / 4));
          }
        } else {
          this.barChartOptions.scales.yAxes[0].ticks.max = 3;
        }
        this._chart.refresh();
      });

  }
}
