import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GetStatistics } from './chart.interface';
import { Observable } from 'rxjs/Observable';
import { BaThemeConfigProvider } from '../../theme';

@Injectable()
export class ChartistJsService {

    private _url = 'http://localhost/census_svr/index.php/report/';
    private headers: any;
    private options: any;

    private _data = {
        simpleBarData: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [15, 24, 43, 27, 5, 10, 23, 44, 68, 50, 26, 8]
                // [13, 22, 49, 22, 4, 6, 24, 46, 57, 48, 22, 4]
            ]
        },
        simpleBarOptions: {
            fullWidth: true,
            height: '300px'
        },
        simplePieData: {
            series: [5, 3, 4]
        },
        simplePieOptions: {
            fullWidth: true,
            height: '300px',
            weight: '300px',
            labelInterpolationFnc: function (value) {
                return Math.round(value / 3 * 100) + '%';
            }
        }
    };

    constructor(private _baConfig: BaThemeConfigProvider, private http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

        this.options = new RequestOptions({ headers: this.headers, withCredentials: false });
    }

    public getBarData(year: any) {
        return this.http.get(this._url + 'api_report/' + year).map(res => res.json());
    }

    public getAll() {
        //console.log(this._data['simpleBarData']);

        return this._data;
    }

    public getResponsive(padding, offset) {
        return [
            ['screen and (min-width: 1550px)', {
                chartPadding: padding,
                labelOffset: offset,
                labelDirection: 'explode',
                labelInterpolationFnc: function (value) {
                    return value;
                }
            }],
            ['screen and (max-width: 1200px)', {
                chartPadding: padding,
                labelOffset: offset,
                labelDirection: 'explode',
                labelInterpolationFnc: function (value) {
                    return value;
                }
            }],
            ['screen and (max-width: 600px)', {
                chartPadding: 0,
                labelOffset: 0,
                labelInterpolationFnc: function (value) {
                    return value[0];
                }
            }]
        ];
    }
}
