import { Injectable } from '@angular/core';
import { GetFliterArea, GetFliterVillage } from './fliter.interface';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class Fliter {

    private _url = 'http://localhost/census_svr/index.php/fliter/';
    private headers: any;
    private options: any;

    constructor(private http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

        this.options = new RequestOptions({ headers: this.headers, withCredentials: false });
    }

    public getRegion() {
        return this.http.get(this._url + 'api_fliter_region').map(res => res.json());

    }

    public getProvince(regionCode: number) {
        return this.http.get(this._url + 'api_fliter_province/' + regionCode)
            .map(res => res.json());
    }

    public getDistrict(provinceCode: number) {
        return this.http.get(this._url + 'api_fliter_district/' + provinceCode)
            .map(res => res.json());
    }

    public getCanton(provinceCode: number, districtCode: number) {
        return this.http.get(this._url + 'api_fliter_canton/' + provinceCode + '/' + districtCode)
            .map(res => res.json());
    }

    public getArea(provinceCode: number, districtCode: number, cantonCode: number, area: number) {
        return this.http.get(this._url + 'api_fliter_area/' + provinceCode + '/'
            + districtCode + '/' + cantonCode + '/' + area).map(res => res.json());
    }

    public getVillage(provinceCode: number, districtCode: number, cantonCode: number,
        areatype: number, area: number) {
        return this.http.get(this._url + 'api_fliter_village/' + provinceCode + '/'
            + districtCode + '/' + cantonCode + '/' + areatype + '/' + area).map(res => res.json());
    }

    public getStatus() {
        return this.http.get('http://localhost/census_svr/index.php/Inputs/getStatus/').map(res => res.json());
    }

    public post_area(newArea: GetFliterArea) {

        let body = `acode=${newArea.areaType}&anum=${newArea.areaCode}&aname=${newArea.areaName}`;

        return this.http.post(this._url + 'addArea', body, this.options);

    }

    public post_village(newVillage: GetFliterVillage) {

        let body = `eaCode=${newVillage.EACode}&vnum=${newVillage.villageNumber}&vname=${newVillage.villageName}`;

        return this.http.post(this._url + 'addVillage', body, this.options);

    }
}
