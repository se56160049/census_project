import { Component, OnInit, OnChanges, Input, ViewChild, NgZone, forwardRef } from '@angular/core';
import {
    FormGroup, AbstractControl, FormBuilder, Validators, FormControl, ControlValueAccessor,
    NG_VALUE_ACCESSOR, NG_VALIDATORS
} from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { Response } from '@angular/http';

import { CommonService } from './../../_services/common.service';
import { ModalComponent } from 'ng2-bs4-modal/ng2-bs4-modal';

import { Fliter } from './fliter.service';
import { InputStatus } from '../inputs/inputs.interface';
import * as Inf from './fliter.interface';

function createFilterValidator() {
    return (c: FormControl) => {
        let err = {
            rangeError: { given: c.value }
        };

        if (c.value['year'].match(/\D/) !== null)
            return err;
        if (c.value['regionId'].match(/^[0]$/) !== null)
            return err;
        if (c.value['provinceId'].match(/^[0]$/) !== null)
            return err;
        if (c.value['districtId'].match(/^[0]$/) !== null)
            return err;
        if (c.value['cantonId'].match(/^[0]$/) !== null)
            return err;
        if (c.value['areaType'].match(/^[0]$/) !== null)
            return err;
        if (c.value['areaId'].match(/^[0]$/) !== null)
            return err;
        if (c.value['eaId'].match(/^[0]$/) !== null)
            return err;

        return null;
    };
}

@Component({
    selector: 'fliter',
    templateUrl: './fliter.html',
    providers: [
        Fliter,
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FliterComponent),
            multi: true,
        }, {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => FliterComponent),
            multi: true
        }
    ]
})

export class FliterComponent implements OnInit, OnChanges, ControlValueAccessor {

    @Input('fliter') _fliter = new Inf.CVAInputFliter;
    @Input('validateOption') _validateOption: boolean;
    @Input('show') _show: boolean;
    @ViewChild('myModal') modal: ModalComponent;
    @ViewChild('myModal1') modal1: ModalComponent;

    get fliter() {
        return this._fliter;
    }

    set fliter(val) {
        // console.log(val);
        this._fliter = val;
        this.propagateChange(val);
    }


    private dataStatus: Observable<Array<InputStatus>>;

    // for change default
    private dataRegion: Array<Inf.GetFliterRegion> = [];
    private dataProvince: Array<Inf.GetFliterProvince> = [];
    private dataDistrict: Array<Inf.GetFliterDistrict> = [];
    private dataCanton: Array<Inf.GetFliterDistrict> = [];
    private dataArea: Array<Inf.GetFliterArea> = [];
    private dataVillage: Array<Inf.GetFliterVillage> = [];

    private form: FormGroup;
    //for search
    private searchNumber: AbstractControl;
    private searchName: AbstractControl;
    private searchStatus: AbstractControl;

    private year: AbstractControl;
    private region: AbstractControl;
    private province: AbstractControl;
    private district: AbstractControl;
    private canton: AbstractControl;
    private areatype: AbstractControl;
    private area: AbstractControl;
    private ea: AbstractControl;
    private village: AbstractControl;

    private newArea: Inf.GetFliterArea = new Inf.GetFliterArea();
    private formAddArea: FormGroup;
    private newAreaNum: AbstractControl;
    private newAreaName: AbstractControl;

    private newVillage: Inf.GetFliterVillage = new Inf.GetFliterVillage();
    private formAddVillage: FormGroup;
    private newVillageEA: AbstractControl;
    private newVillageNum: AbstractControl;
    private newVillageName: AbstractControl;

    private getUpdateUrl = 'http://localhost/census_svr/index.php/Inputs/getFliterData';
    private statusUrl = 'http://localhost/census_svr/index.php/Inputs/getStatus/';

    private validateArea = true;
    private validateVillage = true;
    private errorMessage: string;

    private permission: number;

    constructor(private fb: FormBuilder, private service: Fliter,
        private zone: NgZone, private router: ActivatedRoute, private userService: CommonService) {
        
        
        let yeared = new Date().getFullYear() + 543;
        this.form = this.fb.group({
            searchNumber: [],
            searchName: [],
            searchStatus: ['0'],
            year: [yeared],
            region: ['2'],
            province: ['19'],
            district: ['0'],
            canton: new FormControl({ value: '0', disabled: true }),
            areatype: ['0'],
            area: new FormControl({ value: '0', disabled: true }),
            ea: new FormControl({ value: '0', disabled: true }),
            village: new FormControl({ value: '', disabled: true }),
        });

        this.formAddArea = this.fb.group({
            newAreaNum: ['', Validators.compose([Validators.required, Validators.maxLength(3)])],
            newAreaName: ['', Validators.compose([Validators.required, Validators.maxLength(50)])]
        });

        this.formAddVillage = this.fb.group({
            newVillageEA: ['', Validators.compose([Validators.required, Validators.maxLength(3)])],
            newVillageNum: ['', Validators.compose([Validators.required, Validators.maxLength(3)])],
            newVillageName: ['', Validators.compose([Validators.required, Validators.maxLength(50)])]
        });
        this.searchNumber = this.form.controls['searchNumber'];
        this.searchName = this.form.controls['searchName'];
        this.searchStatus = this.form.controls['searchStatus'];

        this.year = this.form.controls['year'];
        this.region = this.form.controls['region'];
        this.province = this.form.controls['province'];
        this.district = this.form.controls['district'];
        this.canton = this.form.controls['canton'];
        this.areatype = this.form.controls['areatype'];
        this.area = this.form.controls['area'];
        this.ea = this.form.controls['ea'];
        this.village = this.form.controls['village'];

        this.newAreaNum = this.formAddArea.controls['newAreaNum'];
        this.newAreaName = this.formAddArea.controls['newAreaName'];

        this.newVillageEA = this.formAddVillage.controls['newVillageEA'];
        this.newVillageNum = this.formAddVillage.controls['newVillageNum'];
        this.newVillageName = this.formAddVillage.controls['newVillageName'];
    }

    ngOnInit() {
        // Get data for update.
        this.router.params
            .switchMap((params: Params) =>
                this.userService.getDataJsonWithParms(this.getUpdateUrl, params['censusCode']))
            .subscribe((data: any) => {
                if (data[0] !== undefined) {
                    // console.log('update');
                    this.zone.run(() => {
                        this.service.getProvince(data[0].regionId)
                            .subscribe(res => {
                                this.dataProvince = res;
                            });
                        this.service.getDistrict(data[0].provinceId)
                            .subscribe(res => {
                                this.dataDistrict = res;
                            });
                        this.service.getCanton(data[0].provinceId, data[0].districtId)
                            .subscribe(res => {
                                this.dataCanton = res;
                            });
                        this.service.getArea(data[0].provinceId, data[0].districtId,
                            data[0].cantonId, data[0].areaType)
                            .subscribe(res => {
                                this.dataArea = res;
                            });
                        this.service.getVillage(data[0].provinceId, data[0].districtId,
                            data[0].cantonId, data[0].areaType, data[0].areaId)
                            .subscribe(res => {
                                this.zone.run(() => {
                                    this.dataVillage = res;
                                    this.ea.patchValue(data[0].eaId);
                                });
                            });

                    });

                    // Set value.
                    this.year.patchValue(data[0].year);
                    this.region.patchValue(data[0].regionId);
                    this.province.patchValue(data[0].provinceId);
                    this.district.patchValue(data[0].districtId);
                    this.canton.patchValue(data[0].cantonId);
                    this.areatype.patchValue(data[0].areaType);
                    this.area.patchValue(data[0].areaId);
                }
            });
        this.permission = +localStorage.getItem('permission');
        //console.log(this.permission);
        this.dataStatus = this.userService.getDataJson(this.statusUrl);
        this.service.getRegion()
            .subscribe(res => {
                // console.log(res)
                this.dataRegion = res;
            });

        if (this.region.value !== '0') {
            this.selectRegion();
            this.province.reset('19');
            this.province.enable();
            this.selectDistrict();

            this._fliter.regionId = this.region.value;
            this._fliter.provinceId = this.province.value;
            this.propagateChange(this._fliter);
        }

        // Value change.
         this.searchNumber.valueChanges
            .subscribe(data => {
                this._fliter.number = data;
                this.propagateChange(this._fliter);
            });
        
         this.searchName.valueChanges
            .subscribe(data => {
                this._fliter.name = data;
                this.propagateChange(this._fliter);
            });
         this.searchStatus.valueChanges
            .subscribe(data => {
                this._fliter.status = data;
                this.propagateChange(this._fliter);
            });

        this.year.valueChanges
            .subscribe(data => {
                this._fliter.year = data;
                this.propagateChange(this._fliter);
            });

        this.region.valueChanges
            .subscribe(data => {
                if (data !== '0') {
                    this.province.enable();
                    this.province.reset('0');
                } else {
                    this.province.reset('0');
                    this.province.disable();
                }
                this._fliter.regionId = data;
                this.propagateChange(this._fliter);
            });

        this.province.valueChanges
            .subscribe(data => {
                if (data !== '0') {
                    this.district.enable();
                    this.district.reset('0');
                } else {
                    this.district.reset('0');
                    this.district.disable();
                }
                this._fliter.provinceId = data;
                this.propagateChange(this._fliter);
            });

        this.district.valueChanges
            .subscribe(data => {
                if (data !== '0') {
                    this.canton.enable();
                    this.canton.reset('0');
                } else {
                    this.canton.reset('0');
                    this.canton.disable();
                }
                this._fliter.districtId = data;
                this.propagateChange(this._fliter);
            });

        this.canton.valueChanges
            .subscribe(data => {

                if (data !== '0' && this.areatype.value !== '0') {
                    this.selectArea();
                    this.area.enable();
                    this.area.reset('0');
                } else {
                    this.area.reset('0');
                    this.area.disable();
                }
                this._fliter.cantonId = data;
                this.propagateChange(this._fliter);
            });

        this.areatype.valueChanges
            .subscribe(data => {

                if (data !== '0' && this.canton.value !== '0') {
                    this.selectArea();
                    this.area.enable();
                    // if(this.dataArea.length === 1)
                    this.area.setValue('0');
                } else {
                    this.area.reset('0');
                    this.area.disable();
                }

                this._fliter.areaType = data;
                this.propagateChange(this._fliter);
            });

        this.area.valueChanges
            .subscribe(data => {
                if (data !== '0') {
                    this.village.enable();
                    this.ea.enable();
                    // this.village.reset();
                    // this.ea.reset('0');
                } else {
                    this.village.reset();
                    this.ea.reset('0');
                    this.village.disable();
                    this.ea.disable();
                }
                this._fliter.areaId = data;
                this.propagateChange(this._fliter);
            });

        this.ea.valueChanges
            .subscribe(data => {
                // console.log('ea changes');
                // console.log(data);
                if (data !== '0') {
                    // console.log(this.dataVillage);
                    this.zone.run(() => {
                        // console.log('change village name');
                        let temp1 = this.dataVillage.find(findObjects => findObjects.EACode === this.ea.value);
                        this.village.reset(temp1.villageNumber + ': ' + temp1.villageName);
                    });
                } else {
                    this.village.reset('');
                }
                this._fliter.eaId = data;
                this._fliter.villageName = this.village.value;
                this.propagateChange(this._fliter);
                // console.log(this._fliter);
            });


    }

    ngOnChanges(inputs) {
        // console.log(inputs);
        if (inputs._validateOption.currentValue === true) {
            this.zone.runOutsideAngular(() => {
                this.validateFn = createFilterValidator();
                this.propagateChange(this._fliter);
            });
        }
    }

    public writeValue(obj: any) {
        if (obj)
            this.fliter = obj;
    }

    public registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any) { }

    validate(c: FormControl) {
        return this.validateFn(c);
    }

    public selectRegion() {
        this.service.getProvince(this.region.value)
            .subscribe(res => {
                // console.log(res)
                this.dataProvince = res;
            });
        this.province.reset('0');
    }

    public selectDistrict() {
        this.service.getDistrict(this.province.value)
            .subscribe(res => {
                this.dataDistrict = res;
            });
    }

    public selectCanton() {
        this.service.getCanton(this.province.value, this.district.value)
            .subscribe(res => {
                this.dataCanton = res;
            });
    }

    public selectArea() {

        if (this.areatype.value === null || this.canton.value === 0) {

        } else {
            this.zone.run(() => {
                this.service.getArea(this.province.value, this.district.value, this.canton.value, this.areatype.value)
                    .subscribe(res => {
                        this.dataArea = res;
                        if (this.dataArea.length === 1) {
                            for (const key of Object.keys(res)) {
                                this.area.reset(res[key].areaCode);
                            }
                            this.selectVillage();
                        }
                    });
            });
        }
    }

    public selectVillage() {
        this.service.getVillage(this.province.value, this.district.value,
            this.canton.value, this.areatype.value, this.area.value)
            .subscribe(res => {
                this.dataVillage = res;
            });

    }



    public onSubmitArea(): void {

        if (this.formAddArea.valid) {
            let temp = this.dataArea.find(findObjects => findObjects.areaCode === this.newAreaNum.value);
            if (temp === undefined) {
                this.newArea.areaCode = this.province.value + this.district.value
                    + this.canton.value + this.areatype.value + this.newAreaNum.value;
                this.newArea.areaName = this.newAreaName.value;
                this.newArea.areaType = this.areatype.value;

                this.service.post_area(this.newArea)
                    .subscribe((res: Response) => {
                        // console.log(res.status)
                        this.zone.run(() => {
                            this.selectArea();
                            this.area.reset(this.newAreaNum.value);
                            this.newAreaNum.reset();
                            this.newAreaName.reset();
                            this.modal.close();
                            this.validateArea = true;
                            this.selectVillage();
                        });
                    });

            } else {
                this.validateArea = false;
                this.errorMessage = "มีหมายเลขเขตปกครองในฐานข้อมูลแล้ว";
            }
        } else {
            this.validateArea = false;
            this.errorMessage = "กรุณากรอกข้อมูลให้ถูกต้อง";
        }
    }

    public onSubmitVillage(): void {
        if (this.formAddVillage.valid) {
            let temp = this.dataVillage.find(findObjects => findObjects.EACode === this.newVillageEA.value);
            if (temp === undefined) {
                this.newVillage.EACode = this.province.value + this.district.value
                    + this.canton.value + this.areatype.value
                    + this.area.value + this.newVillageEA.value;
                this.newVillage.villageNumber = this.newVillageNum.value;
                this.newVillage.villageName = this.newVillageName.value;

                this.service.post_village(this.newVillage)
                    .subscribe((res: Response) => {
                        this.zone.run(() => {
                            this.selectVillage();
                            this.newVillageEA.reset();
                            this.newVillageNum.reset();
                            this.newVillageName.reset();
                            this.modal1.close();
                            this.validateVillage = true;
                        });
                    });

            } else {
                this.validateVillage = false;
                this.errorMessage = "มีหมายเลขแจ้งนับในฐานข้อมูลแล้ว";
            }
        } else {
            this.validateVillage = false;
            this.errorMessage = "กรุณากรอกข้อมูลให้ถูกต้อง";
        }
    }

    public close() {
        this.modal.close();
    }

    public open() {
        this.modal.open();
    }

    // the method set in registerOnChange to emit changes back to the form
    propagateChange = (_: any) => { };
    validateFn = (_: any) => { };
}
