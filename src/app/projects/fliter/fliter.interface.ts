export class CVAInputFliter {
    public number: string = '';
    public name: string = '';
    public status: string = '0';
    public year: string = '2560';
    public regionId: string = '2';
    public provinceId: string = '19';
    public districtId: string = '0';
    public cantonId: string = '0';
    public areaType: string = '0';
    public areaId: string = '0';
    public villageName: string = '0';
    public eaId: string = '0';
}

export class GetFliterRegion {
    public regionId?: number;
    public regionCode?: number;
    public regionName?: string;
}

export class GetFliterProvince {
    public provinceId?: number;
    public provinceCode?: number;
    public provinceName?: string;
}

export class GetFliterDistrict {
    public districtId?: number;
    public districtCode?: number;
    public districtName?: string;
}

export class GetFliterCanton {
    public cantonId?: number;
    public cantonCode?: number;
    public cantonName?: string;
}

export class GetFliterArea {
    public areaId?: number;
    public areaType?: number;
    public areaCode?: number;
    public areaName?: string;
}

export class GetFliterVillage {
    public villageId?: number;
    public EACode?: number;
    public villageNumber?: number;
    public villageName?: string;
}