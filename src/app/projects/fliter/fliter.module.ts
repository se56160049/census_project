import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { ModalModule } from 'ng2-bs4-modal/ng2-bs4-modal';

import { FliterComponent } from './fliter.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgaModule,
        ModalModule
    ],
    declarations: [
        FliterComponent
    ],
    exports: [
        FliterComponent
    ]
})

export class FliterModule { }
