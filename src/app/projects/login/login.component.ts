import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from './../../_services/authentication.service';

import 'style-loader!./login.scss';

@Component({
  selector: 'login',
  templateUrl: './login.html'
})
export class Login implements OnInit {

  public form: FormGroup;
  public username: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;
  // Alert.
  private alerts: any = [];

  constructor(fb: FormBuilder, private router: Router,
    private authenticationService: AuthenticationService) {

    this.form = fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.username = this.form.controls['username'];
    this.password = this.form.controls['password'];
  }

  ngOnInit() {
    // this.authenticationService.logout();
  }

  public login() {
    this.submitted = true;
    if (this.form.valid) {
      // your code goes here
      this.authenticationService.login(this.username.value, this.password.value)
        .subscribe(result => {
          if (result === true) {
            // login successful
            this.router.navigate(['/project/search']);
          } else {
            // login failed
            this.alerts = [];
            this.alerts.push({
              type: 'danger',
              msg: `Username หรือ Password ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง`,
              timeout: 10000
            });
          }
        });

    }
  }
}
