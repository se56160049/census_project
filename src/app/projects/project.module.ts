import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './project.routing';
import { NgaModule } from '../theme/nga.module';

import { Project } from './project.component';
import { AdminGuard, ManagerGuard } from './../_guards/index';

@NgModule({
    imports: [CommonModule, NgaModule, routing],
    declarations: [Project],
    providers: [AdminGuard, ManagerGuard]
})
export class ProjectModule {
}
