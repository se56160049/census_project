import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgaModule } from '../../theme/nga.module';
import { routing } from './tables.routing';
import { TablesComponent } from './tables.component';
import { AlertModule } from 'ng2-bootstrap/alert';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableService } from './tables.service';

@NgModule({
    imports: [
        CommonModule,
        NgaModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        AlertModule.forRoot()
    ],
    declarations: [
        TablesComponent
    ],
  providers: [
         TableService

  ]
})

export class TablesModule { }