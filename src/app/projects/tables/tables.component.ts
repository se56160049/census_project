import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl, ControlValueAccessor, } from '@angular/forms';
import { CommonService } from './../../_services/common.service';
import { TableService } from './tables.service';
import { GetStatistics } from './tables.interface';
import { AlertModule } from 'ng2-bootstrap/alert';
@Component({
    selector: 'tables',
    templateUrl: './tables.html',
    styleUrls: ['./../../theme/components/baContentTop/baContentTop.scss'],
    providers: [CommonService]
})

export class TablesComponent implements OnInit {

    private dataStatistics: Array<GetStatistics> = []
    public toggle: boolean = false;
    private checkToggle: number;
    private allYear = [];
    private allProvince = [];
    private allDistrict = [];
    private allCanton = [];
    private presentYear = new Date().getFullYear() + 543;
    private formReport: FormGroup;
    private select_year: AbstractControl;
    private select_year_to: AbstractControl;
    private select_province: AbstractControl;
    private select_district: AbstractControl;
    private select_canton: AbstractControl;
    private alerts: any = [];
    constructor(private router: ActivatedRoute, private _chartistJsService: TableService,
        private reportService: CommonService, private fb: FormBuilder, private zone: NgZone) {

        //console.log("constructorreport");
        this.formReport = this.fb.group({

            select_year: [(this.presentYear) - 1],
            select_year_to: [this.presentYear],
            select_province: ['19'],
            select_district: ['0'],
            select_canton: ['0']
        });

        this.select_year = this.formReport.controls['select_year'];
        this.select_year_to = this.formReport.controls['select_year_to'];
        this.select_province = this.formReport.controls['select_province'];
        this.select_district = this.formReport.controls['select_district'];
        this.select_canton = this.formReport.controls['select_canton'];

    }

    ngOnInit() {
        this.selectYear();
        this.getTableActivity();

        this.select_province.valueChanges
            .subscribe(data => {
                if (data !== '0') {
                    this.select_district.enable();
                    this.select_district.reset('0');
                } else {
                    this.select_district.reset('0');
                    this.select_district.disable();
                }
            });

        this.select_district.valueChanges
            .subscribe(data => {
                if (data !== '0') {
                    this.select_canton.enable();
                    this.select_canton.reset('0');
                } else {
                    this.select_canton.reset('0');
                    this.select_canton.disable();
                }
            });
    }

    public selectYear() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_year_reports/')
            .subscribe(res => {
                this.allYear = res;
            });

        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_province/')
            .subscribe(res => {
                this.allProvince = res;
            });

        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_district/' + this.select_province.value)
            .subscribe(res => {
                this.allDistrict = res;
            });

    }

    public getCanton() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_canton/' + this.select_province.value + '/' + this.select_district.value)
            .subscribe(res => {
                this.allCanton = res;
            });
        if (this.toggle == false) {
            this.getTableActivity();
        } else {
            this.getTableStatus();
        }

    }

    public getTableActivity() {
        this.toggle = false
        // console.log(this.toggle)
        if (this.select_district.value != 0 && this.select_canton.value != 0) {
            this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_table_district_canton/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value + '/' + this.select_canton.value).subscribe(res => {
                // this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_table_district/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value).subscribe(res => {
                this.dataStatistics = res;
                let i = 0;
                res.forEach(e => {
                    if (+e.num1 === 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = 0;
                    } else if (+e.num1 === 0 && +e.num2 != 0) {
                        this.dataStatistics[i++]['total'] = (+e.num2 * (-100));
                    } else if (+e.num1 != 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = (+e.num1 * 100);
                    } else {
                        this.dataStatistics[i++]['total'] = ((100 - ((100 / (+e.num2)) * (+e.num1))) * (-1));
                    }

                });
            });
        } else if (this.select_district.value != 0) {
            this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_table_district/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value).subscribe(res => {
                // this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_table/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value).subscribe(res => {
                this.dataStatistics = res;
                let i = 0;
                res.forEach(e => {
                    if (+e.num1 === 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = 0;
                    } else if (+e.num1 === 0 && +e.num2 != 0) {
                        this.dataStatistics[i++]['total'] = (+e.num2 * (-100));
                    } else if (+e.num1 != 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = (+e.num1 * 100);
                    } else {
                        this.dataStatistics[i++]['total'] = ((100 - ((100 / (+e.num2)) * (+e.num1))) * (-1));
                    }

                });
            });
        } else {
            this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_table/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value).subscribe(res => {
                // this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_table_district_canton/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value + '/' + this.select_canton.value).subscribe(res => {            
                this.dataStatistics = res;
                let i = 0;
                res.forEach(e => {
                    if (+e.num1 === 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = 0;
                    } else if (+e.num1 === 0 && +e.num2 != 0) {
                        this.dataStatistics[i++]['total'] = (+e.num2 * (-100));
                    } else if (+e.num1 != 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = (+e.num1 * 100);
                    } else {
                        this.dataStatistics[i++]['total'] = ((100 - ((100 / (+e.num2)) * (+e.num1))) * (-1));
                    }

                });
            });
        }

        if (this.toggle == false) {
            if (this.select_district.value != 0 && this.select_canton.value != 0) {
                this.checkToggle = 3;
                this.getCountStatisticsC();
                // console.log(this.checkToggle)
            } else if (this.select_district.value != 0) {
                this.checkToggle = 2;
                this.getCountStatisticsD();
                // console.log(this.checkToggle)
            } else {
                this.checkToggle = 1;
                this.getCountStatistics();
                //    console.log(this.checkToggle)
            }
        }

    }

    public getCountStatus() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_countStatus/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value)
            .subscribe(res => {
                this.alerts = [];
                this.alerts.push({
                    type: 'info',
                    status: res[0].status,
                    StatusLastYear: res[0].StatusLastYear,
                    pName: res[0].pName
                });
            });
    }

    public getCountStatusD() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_countStatusD/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value)
            .subscribe(res => {
                this.alerts = [];
                this.alerts.push({
                    type: 'info',
                    status: res[0].status,
                    StatusLastYear: res[0].StatusLastYear,
                    pName: res[0].pName,
                    dName: res[0].dName
                });
            });
    }

    public getCountStatusC() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_countStatusC/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value + '/' + this.select_canton.value)
            .subscribe(res => {
                this.alerts = [];
                this.alerts.push({
                    type: 'info',
                    status: res[0].status,
                    StatusLastYear: res[0].StatusLastYear,
                    pName: res[0].pName,
                    dName: res[0].dName,
                    cName: res[0].cName
                });
            });
    }

    public getCountStatistics() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_countStatistics/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value)
            .subscribe(res => {
                this.alerts = [];
                this.alerts.push({
                    type: 'info',
                    status: res[0].status,
                    StatusLastYear: res[0].StatusLastYear,
                    pName: res[0].pName
                });
            });
    }

    public getCountStatisticsD() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_countStatisticsD/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value)
            .subscribe(res => {
                this.alerts = [];
                this.alerts.push({
                    type: 'info',
                    status: res[0].status,
                    StatusLastYear: res[0].StatusLastYear,
                    pName: res[0].pName,
                    dName: res[0].dName
                });
            });
    }

    public getCountStatisticsC() {
        this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_countStatisticsC/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value + '/' + this.select_canton.value)
            .subscribe(res => {
                this.alerts = [];
                this.alerts.push({
                    type: 'info',
                    status: res[0].status,
                    StatusLastYear: res[0].StatusLastYear,
                    pName: res[0].pName,
                    dName: res[0].dName,
                    cName: res[0].cName
                });
            });
    }

    public getTableStatus() {
        this.toggle = true
        // console.log(this.toggle)
        if (this.select_district.value != 0 && this.select_canton.value != 0) {
            this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_statusTable_district_canton/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value + '/' + this.select_canton.value).subscribe(res => {
                this.dataStatistics = res;
                let i = 0;
                res.forEach(e => {
                    if (+e.num1 === 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = 0;
                    } else if (+e.num1 === 0 && +e.num2 != 0) {
                        this.dataStatistics[i++]['total'] = (+e.num2 * (-100));
                    } else if (+e.num1 != 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = (+e.num1 * 100);
                    } else {
                        this.dataStatistics[i++]['total'] = ((100 - ((100 / (+e.num2)) * (+e.num1))) * (-1));
                    }

                });
            });
        } else if (this.select_district.value != 0) {
            this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_statusTable_district/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value + '/' + this.select_district.value).subscribe(res => {
                this.dataStatistics = res;
                let i = 0;
                res.forEach(e => {
                    if (+e.num1 === 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = 0;
                    } else if (+e.num1 === 0 && +e.num2 != 0) {
                        this.dataStatistics[i++]['total'] = (+e.num2 * (-100));
                    } else if (+e.num1 != 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = (+e.num1 * 100);
                    } else {
                        this.dataStatistics[i++]['total'] = ((100 - ((100 / (+e.num2)) * (+e.num1))) * (-1));
                    }

                });
            });
        } else {
            this.reportService.getDataJson('http://localhost/census_svr/index.php/report/api_report_statusTable/' + this.select_year.value + '/' + this.select_year_to.value + '/' + this.select_province.value).subscribe(res => {
                this.dataStatistics = res;
                let i = 0;
                res.forEach(e => {
                    if (+e.num1 === 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = 0;
                    } else if (+e.num1 === 0 && +e.num2 != 0) {
                        this.dataStatistics[i++]['total'] = (+e.num2 * (-100));
                    } else if (+e.num1 != 0 && +e.num2 === 0) {
                        this.dataStatistics[i++]['total'] = (+e.num1 * 100);
                    } else {
                        this.dataStatistics[i++]['total'] = ((100 - ((100 / (+e.num2)) * (+e.num1))) * (-1));
                    }

                });
            });
        }

        if (this.toggle == true) {
            if (this.select_district.value != 0 && this.select_canton.value != 0) {
                this.checkToggle = 6;
                this.getCountStatusC();
                // console.log(this.checkToggle)
            } else if (this.select_district.value != 0) {
                this.checkToggle = 5;
                this.getCountStatusD();
                // console.log(this.checkToggle)
            } else {
                this.checkToggle = 4;
                this.getCountStatus();
                //    console.log(this.checkToggle)
            }
        }
    }

    public randomize() {
        if (this.toggle === true) {
            this.getTableActivity();
            this.toggle = false;

        } else {
            this.getTableStatus();
            this.toggle = true;
        }
    }

}
