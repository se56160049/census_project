import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { TablesComponent } from './tables.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
    {
        path: '',
        component: TablesComponent,
        children: [
            //{ path: 'treeview', component: TreeViewComponent }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
