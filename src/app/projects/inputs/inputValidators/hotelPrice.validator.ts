import { FormGroup, AbstractControl } from '@angular/forms';

export class HotelPriceValidator {

    public static validate(firstField, secondField) {
        return (c: FormGroup) => {
            if (c.controls && c.controls[firstField].value > c.controls[secondField].value) {
                return {
                    hotelsInvalid: { valid: false }
                };
            } else {
                return null;
            }
        };
    }
}
