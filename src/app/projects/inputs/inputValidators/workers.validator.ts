import { FormGroup, AbstractControl } from '@angular/forms';

export class WorkersValidator {

    public static validate(firstField, secondField, issue) {
        return (c: FormGroup) => {
            if (c.controls
                && c.controls[firstField].value === c.controls[secondField].value
                && c.controls[issue].value !== '2') {
                return {
                    workerInvalid: { valid: false }
                };

            } else if (c.controls && c.controls[firstField].value < c.controls[secondField].value) {
                return {
                    workerInvalid: { valid: false }
                };
            } else {
                return null;
            }
        };
    }
}
