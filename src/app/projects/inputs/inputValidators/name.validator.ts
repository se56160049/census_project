import { FormGroup, AbstractControl, Validators } from '@angular/forms';

export class NameValidator {

    public static validate(firstField, secondField, thirdField) {
        return (c: FormGroup) => {
            if (c.controls
                && c.controls[firstField].value === '' && c.controls[secondField].value === '' &&
                c.controls[thirdField].value === '') {
                c.controls[firstField].setValidators(Validators.required);
                c.controls[secondField].setValidators(Validators.required);
                c.controls[thirdField].setValidators(Validators.required);
            } else {
                return null;
            }
        };
    }
}
