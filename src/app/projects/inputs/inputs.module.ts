import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { AlertModule } from 'ng2-bootstrap/alert';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { FliterModule } from './../fliter/fliter.module';
import { routing } from './inputs.routing';
import { Inputs } from './inputs.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgaModule,
        routing,
        ReactiveFormsModule,
        FliterModule,
        NguiAutoCompleteModule,
        AlertModule.forRoot()
    ],
    declarations: [
        Inputs
    ]
})

export class InputsModule { }

