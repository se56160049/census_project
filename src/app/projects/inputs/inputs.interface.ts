export class InputStatus {
    private statusId?: number;
    private sName?: any;
}

export class InputActivity {
    private activityId?: number;
    private aName?: any;
}

export class InputFormatLaw {
    private flId?: number;
    private flName?: any;
}

export class InputFormatEconomy {
    private feId?: number;
    private feName?: any;
}

export class InputTSIC {
    public tsic?: number;
    public tsName?: any;
}

export class InputUsers {
    public id?: any;
    public name?: any;
}

export class InsertInputs {
    public road: string = '';
    public alley: string = '';
    public place: string = '';
    public aNum: string = '';
    public tName: string = '';
    public fName: string = '';
    public lName: string = '';
    public facName: string = '';
    public status: number = 0;
    public otherStatus?: string = '';
    public ecoActivity?: number = 0;
    public typeActivity?: string = '';
    public formatEconomy?: number = 0;
    public otherFormatEconomy?: string = '';
    public formatLaw?: number = 0;
    public otherFormatLaw?: string = '';
    public regisType?: number = 0;
    public personalId?: string = '';
    public nationPartner?: number = 0;
    public totalWorkers?: number = 0;
    public workers?: number = 0;
    public yearActivity?: number = 0;
    public hospitalBed?: number = 0;
    public hType?: number = 0;
    public hRoom?: number = 0;
    public hMinPrice?: number = 0;
    public hMaxPrice?: number = 0;
    public useComputer?: number = 0;
    public useInternet?: number = 0;
    // checkboxModel need add.
    public useEService?: string = '1';
    public tel?: string = '';
    public web?: string = '';
    public email?: string = '';
    // private censusId?: string = '';
    public tsicId?: string = '';
    public mbTitleName?: string = '';
    public mbFName?: string = '';
    public mbLName?: string = '';
    public mbDeptName?: string = '';
    public mbRoad?: string = '';
    public mbAlley?: string = '';
    public mbPlace?: string = '';
    public mbANum?: string = '';
    public mbProvince?: string = '0';
    public mbDistrict?: string = '0';
    public mbCanton?: string = '0';
    public note?: string = '';
    public operatorId: string = '';
    public editorialStaffId: string = '';
    public recorderStaffId: string = '';
    public examinerId: string = '';
}
