import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

import { WorkersValidator, NameValidator, HotelPriceValidator } from './inputValidators/index';
import { FliterComponent } from './../fliter/fliter.component';
import { CommonService } from './../../_services/common.service';
import * as Inf from './inputs.interface';
import { GetFliterProvince, GetFliterDistrict, GetFliterCanton } from './../fliter/fliter.interface';
import { CVAInputFliter } from './../fliter/fliter.interface';

import 'style-loader!./../login/login.scss';

@Component({
    selector: 'inputs',
    templateUrl: './inputs.html',
    styleUrls: ['./../../theme/components/baContentTop/baContentTop.scss'],
    providers: [WorkersValidator]
})

export class Inputs implements OnInit {

    // url
    private statusUrl = 'http://localhost/census_svr/index.php/Inputs/getStatus/';
    private activityUrl = 'http://localhost/census_svr/index.php/Inputs/getActivity';
    private formatLawUrl = 'http://localhost/census_svr/index.php/Inputs/getFormatLaw';
    private formatEconomyUrl = 'http://localhost/census_svr/index.php/Inputs/getFormatEconomy';
    private nationPartnerUrl = 'http://localhost/census_svr/index.php/Inputs/getNationPartner';
    private tsicUrl = 'http://localhost/census_svr/index.php/Inputs/getTSIC';
    private userUrl = 'http://localhost/census_svr/index.php/Inputs/getUsers';
    private insertUrl = 'http://localhost/census_svr/index.php/Inputs/insertInputs';
    private getUpdateUrl = 'http://localhost/census_svr/index.php/Inputs/getInputData';
    private provinceUrl = 'http://localhost/census_svr/index.php/fliter/getGeneralProvince';
    private districtUrl = 'http://localhost/census_svr/index.php/fliter/api_fliter_district';
    private cantonUrl = 'http://localhost/census_svr/index.php/fliter/api_fliter_canton';

    // initial get data for fliter and selector.
    private getFliter = new CVAInputFliter;
    private getValidate = true;
    private getStatus: Observable<Array<Inf.InputStatus>>;
    private getActivity: Observable<Array<Inf.InputActivity>>;
    private getFormatLaw: Observable<Array<Inf.InputFormatLaw>>;
    private getFormatEconomy: Observable<Array<Inf.InputFormatEconomy>>;
    private getTSIC: Array<Inf.InputTSIC>;
    private getUsers: Array<Inf.InputUsers>;
    private getProvince: Observable<Array<GetFliterProvince>>;
    private getDistrict: Array<GetFliterDistrict>;
    private getCanton: Array<GetFliterCanton>;
    private objFeildData = new Inf.InsertInputs;
    private censusCode: string;
    private topicName: string = 'เพิ่มข้อมูลพื้นฐาน';
    private baTop: string = 'Industry Information';
    // checkbox.
    private checkboxModel = [{
        name: '1 : ขายผ่านเว็บไซต์ของตัวเอง',
        checked: false,
        disabled: true,
        class: 'col-md-3',
        id: 1
    }, {
        name: '2 : ขายผ่านเว็บไซต์ของคนอื่น',
        checked: false,
        disabled: true,
        class: 'col-md-3',
        id: 2
    }, {
        name: '3 : ขายผ่านตลาดกลางอิเล็กทรอนิกส์',
        checked: false,
        disabled: true,
        class: 'col-md-3',
        id: 3
    }, {
        name: '4 : ขายผ่าน Social Network',
        checked: false,
        disabled: true,
        class: 'col-md-3',
        id: 4
    }];

    // Alert.
    private alerts: any = [];

    // property of form.
    // tab 1  fliter.
    private fliter: AbstractControl;
    // tab 2
    // row 1
    private form: FormGroup;
    private road: AbstractControl;
    private alley: AbstractControl;
    private place: AbstractControl;
    private aNum: AbstractControl;
    // row 2
    private tName: AbstractControl;
    private names: FormGroup;
    private fName: AbstractControl;
    private lName: AbstractControl;
    private facName: AbstractControl;
    // row 3
    private status: AbstractControl;
    private otherStatus: AbstractControl;
    // row 4
    private ecoActivity: AbstractControl;
    private typeActivity: AbstractControl;
    // row 6
    private formatLaw: AbstractControl;
    // row 5
    private formatEconomy: AbstractControl;
    private otherFormatLaw: AbstractControl;
    private otherFormatEconomy: AbstractControl;
    private regisType: AbstractControl;
    private personalId: AbstractControl;
    private nationPartner: AbstractControl;
    private aboutWorker: FormGroup;
    private totalWorkers: AbstractControl;
    private workers: AbstractControl;
    private yearActivity: AbstractControl;
    private aboutHotels: FormGroup;
    private hType: AbstractControl;
    private hRoom: AbstractControl;
    private hMinPrice: AbstractControl;
    private hMaxPrice: AbstractControl;
    private hospitalBed: AbstractControl;
    private useComputer: AbstractControl;
    private useInternet: AbstractControl;
    private useEService: AbstractControl;
    private tel: AbstractControl;
    private web: AbstractControl;
    private email: AbstractControl;
    // private censusId: AbstractControl;
    private tsicId: AbstractControl;

    private mbTitleName: AbstractControl;
    private deptNames: FormGroup;
    private mbFName: AbstractControl;
    private mbLName: AbstractControl;
    private mbDeptName: AbstractControl;
    private mbRoad: AbstractControl;
    private mbAlley: AbstractControl;
    private mbPlace: AbstractControl;
    private mbANum: AbstractControl;
    private mbProvince: AbstractControl;
    private mbDistrict: AbstractControl;
    private mbCanton: AbstractControl;
    private note: AbstractControl;
    // tab 3
    private operatorId: AbstractControl;
    private editorialStaffId: AbstractControl;
    private recorderStaffId: AbstractControl;
    private examinerId: AbstractControl;
    // property of form.

    constructor(private fb: FormBuilder, private userService: CommonService,
        private router: ActivatedRoute, private routing: Router, private _sanitizer: DomSanitizer) {

        this.form = this.fb.group({
            'fliter': [this.getFliter],
            'road': [this.objFeildData.road],
            'alley': [this.objFeildData.alley],
            'place': [this.objFeildData.place],
            'aNum': [this.objFeildData.aNum, Validators.compose(
                [Validators.required, Validators.pattern('^[0-9/]{1,}$')])],
            'tName': [this.objFeildData.tName, Validators.compose(
                [Validators.required, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')])],
            'names': this.fb.group({
                'fName': [this.objFeildData.fName, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')],
                'lName': [this.objFeildData.lName, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')],
                'facName': [this.objFeildData.facName],
            }, { validator: NameValidator.validate('fName', 'lName', 'facName') }),
            'status': [this.objFeildData.status, Validators.compose(
                [Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')])],
            'otherStatus': [this.objFeildData.otherStatus],
            'ecoActivity': [this.objFeildData.ecoActivity],
            'typeActivity': [this.objFeildData.typeActivity],
            'formatEconomy': [this.objFeildData.formatEconomy],
            'otherFormatLaw': [this.objFeildData.otherFormatLaw],
            'otherFormatEconomy': [this.objFeildData.otherFormatEconomy],
            'regisType': [this.objFeildData.regisType],
            'personalId': [this.objFeildData.personalId],
            'nationPartner': [this.objFeildData.nationPartner],
            'aboutWorker': this.fb.group({
                'formatLaw': [this.objFeildData.formatLaw],
                'totalWorkers': [this.objFeildData.totalWorkers],
                'workers': [this.objFeildData.workers],
            }),
            'yearActivity': [this.objFeildData.yearActivity],
            'hType': [this.objFeildData.hType],
            'hRoom': [this.objFeildData.hRoom],
            'aboutHotels': this.fb.group({
                'hMinPrice': [this.objFeildData.hMinPrice],
                'hMaxPrice': [this.objFeildData.hMaxPrice],
            }),
            'hospitalBed': [this.objFeildData.hospitalBed],
            'useComputer': [this.objFeildData.useComputer],
            'useInternet': [this.objFeildData.useInternet],
            'useEService': [this.objFeildData.useEService],
            'tel': [this.objFeildData.tel, Validators.pattern(`(02-[0-9]{7}|[0-9]{3}-[0-9]{7})`)],
            'web': [this.objFeildData.web, Validators.pattern(
                `^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$`)],
            'email': [this.objFeildData.email, Validators.pattern(
                `^[a-zA-Z0-9.!#$%&'*+=?^_\`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$`)],
            'tsicId': [this.objFeildData.tsicId],
            'mbTitleName': [this.objFeildData.mbTitleName],
            'deptNames': this.fb.group({
                'mbFName': [this.objFeildData.mbFName],
                'mbLName': [this.objFeildData.mbLName],
                'mbDeptName': [this.objFeildData.mbDeptName],
            }),
            'mbRoad': [this.objFeildData.mbRoad],
            'mbAlley': [this.objFeildData.mbAlley],
            'mbPlace': [this.objFeildData.mbPlace],
            'mbANum': [this.objFeildData.mbANum],
            'mbProvince': [this.objFeildData.mbProvince],
            'mbDistrict': [this.objFeildData.mbDistrict],
            'mbCanton': [this.objFeildData.mbCanton],
            'note': [this.objFeildData.note],
            'operatorId': [this.objFeildData.operatorId, Validators.required],
            'editorialStaffId': [this.objFeildData.editorialStaffId, Validators.required],
            'recorderStaffId': [this.objFeildData.recorderStaffId, Validators.required],
            'examinerId': [this.objFeildData.examinerId, Validators.required],
        });

        // created AbstractControl
        this.createdAbControl();
    }

    ngOnInit() {

        // get Data from APIs.
        this.getStatus = this.userService.getDataJson(this.statusUrl);
        this.getActivity = this.userService.getDataJson(this.activityUrl);
        this.getFormatLaw = this.userService.getDataJson(this.formatLawUrl);
        this.getFormatEconomy = this.userService.getDataJson(this.formatEconomyUrl);
        this.userService.getDataJson(this.userUrl).subscribe(data => { this.getUsers = data; });

        // Get data for update.
        this.router.params
            .switchMap((params: Params) =>
                this.userService.getDataJsonWithParms(this.getUpdateUrl, this.censusCode = params['censusCode']))
            .subscribe((data: any) => {
                if (data[0] !== undefined) {
                    this.topicName = 'แก้ไขข้อมูลพื้นฐาน';
                    this.baTop = 'Update';
                    this.form.patchValue(data[0]);
                    this.fName.patchValue(data[0].fName);
                    this.lName.patchValue(data[0].lName);
                    this.facName.patchValue(data[0].facName);
                    this.formatLaw.patchValue(data[0].formatLaw);
                    this.totalWorkers.patchValue(data[0].totalWorkers);
                    this.workers.patchValue(data[0].workers);
                    this.regisType.patchValue(data[0].regisType);
                    this.hMinPrice.patchValue(data[0].hMinPrice);
                    this.hMaxPrice.patchValue(data[0].hMaxPrice);
                    this.mbFName.patchValue(data[0].mbFName);
                    this.mbLName.patchValue(data[0].mbLName);
                    this.mbDeptName.patchValue(data[0].mbDeptNameY);
                    // set checkbox.
                    this.checkboxModel[0].checked = data[0].saleOwnWeb === '1' ? true : false;
                    this.checkboxModel[1].checked = data[0].saleOtherWeb === '1' ? true : false;
                    this.checkboxModel[2].checked = data[0].saleEmarket === '1' ? true : false;
                    this.checkboxModel[3].checked = data[0].saleSocial === '1' ? true : false;
                }
            });

        // Major change for set validatates.
        this.status.valueChanges
            .subscribe(data => {
                if (data === '1' || data === '2') {
                    this.ecoActivity.setValidators(Validators.compose(
                        [Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')]));
                    this.typeActivity.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')]));

                    if (data === '1') {

                        this.formatLaw.setValidators(Validators.compose(
                            [Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')]));

                        this.formatEconomy.setValidators(Validators.compose(
                            [Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')]));

                        this.aboutWorker.setValidators(WorkersValidator.
                            validate('totalWorkers', 'workers', 'formatLaw'));
                        this.totalWorkers.setValidators(
                            Validators.compose([Validators.required, Validators.pattern('^[1-9]{1}[0-9]{0,5}$')]));
                        this.workers.setValidators(
                            Validators.compose([Validators.required, Validators.pattern('^[0-9]{0,6}$')]));
                    } else {
                        this.clearValidateStatus();
                    }
                } else if (data === '8') {
                    this.otherStatus.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')]));
                } else {
                    // Clear validates.
                    this.ecoActivity.clearValidators();
                    this.ecoActivity.updateValueAndValidity();
                    this.typeActivity.clearValidators();
                    this.typeActivity.updateValueAndValidity();
                    this.clearValidateStatus();

                    // Check WTF is invalid in form.
                    for (let propertyName in this.form['controls']) {
                        if (this.form.controls[propertyName]) {
                            console.log(propertyName + ' : ' + this.form.controls[propertyName].valid || 'nulll');
                        }
                    }
                }
            });

        // config factor name.
        this.fName.valueChanges
            .subscribe(data => {
                if (data !== '')
                    this.facName.disable();
                else
                    this.facName.enable();
            });
        this.lName.valueChanges
            .subscribe(data => {
                if (data !== '')
                    this.facName.disable();
                else
                    this.facName.enable();
            });

        // this.facName.valueChanges
        //     .subscribe(() => {
        //         this.facName.markAsPending();
        //         this.facName.markAsTouched();
        //     });


        this.mbFName.valueChanges
            .subscribe(data => {
                if (data !== '')
                    this.mbDeptName.disable();
                else
                    this.mbDeptName.enable();
            });
        this.mbLName.valueChanges
            .subscribe(data => {
                if (data !== '')
                    this.mbDeptName.disable();
                else
                    this.mbDeptName.enable();
            });

        // this.mbDeptName.valueChanges
        //     .subscribe(() => {
        //         this.mbDeptName.markAsPending();
        //         this.mbDeptName.markAsTouched();
        //     });


        this.formatLaw.valueChanges
            .subscribe(data => {
                if (this.formatLaw.value === '1') {
                    this.regisType.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')]));
                } else if (this.formatLaw.value === '9') {
                    this.otherFormatLaw.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')]));
                } else if (this.formatLaw.value <= '3' && this.formatLaw.value > '0') {
                    this.personalId.setValidators(
                        Validators.compose([Validators.required, Validators.pattern('^([1]|[0-9]{13})$')]));
                    if (this.formatLaw.value === '3') {
                        this.nationPartner.setValidators(Validators.compose([
                            Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')]));
                    }
                } else {
                    this.regisType.clearValidators();
                    this.otherFormatLaw.clearValidators();
                    this.personalId.clearValidators();
                    this.nationPartner.clearValidators();
                }
            });

        // config formatEconomy.
        this.formatEconomy.valueChanges
            .subscribe(data => {
                data = 4 ? this.otherFormatEconomy.reset('') : '';
                if (this.formatEconomy.value === '4') {
                    this.otherFormatEconomy.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')]));
                } else if (this.formatEconomy.value === '3') {
                    this.getProvince = this.userService.getDataJson(this.provinceUrl);

                    this.deptNames.setValidators(NameValidator.validate('mbFName', 'mbLName', 'mbDeptName'));
                    this.mbTitleName.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$')]));
                    this.mbFName.setValidators(Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$'));
                    this.mbLName.setValidators(Validators.pattern('^[0-9a-zA-Zก-ฮะ-์./ ]{1,}$'));
                    this.mbANum.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[0-9/]{1,}$')]));
                    this.mbProvince.setValidators(Validators.required);
                    this.mbDistrict.setValidators(Validators.required);
                    this.mbCanton.setValidators(Validators.required);
                } else {
                    this.deptNames.clearValidators();
                    // this.deptNames.updateValueAndValidity();
                    this.otherFormatEconomy.clearValidators();
                    this.mbTitleName.clearValidators();
                    this.mbFName.clearValidators();
                    this.mbLName.clearValidators();
                    this.mbANum.clearValidators();
                    this.mbProvince.clearValidators();
                    this.mbDistrict.clearValidators();
                    this.mbCanton.clearValidators();
                }
            });

        this.useEService.valueChanges
            .subscribe(() => {
                if (this.useEService.value === '0') {
                    this.checkboxModel.forEach(element => {
                        element.disabled = false;
                        element.checked = false;
                    });
                } else {
                    this.checkboxModel.forEach(element => {
                        element.disabled = true;
                        element.checked = false;
                    });
                }
            });

        // config getTSIC.
        this.ecoActivity.valueChanges
            .subscribe(() => {
                this.userService.getDataJsonWithParms(this.tsicUrl, this.ecoActivity.value)
                    .subscribe((data) => {
                        this.getTSIC = data;
                    });
                if (this.ecoActivity.value === '8') {
                    this.hType.setValidators(Validators.compose([
                        Validators.required, Validators.pattern('^[1-9]{1}[0-9]*$')]));
                } else {
                    this.hType.clearValidators();
                }
            });

        this.hType.valueChanges
            .subscribe(() => {
                if (this.hType.value === '1') {
                    // console.log('htype = 1');
                    this.aboutHotels.setValidators(HotelPriceValidator.validate('hMinPrice', 'hMaxPrice'));
                } else {
                    this.aboutHotels.clearValidators();
                }
            });

        this.mbProvince.valueChanges
            .subscribe(data => {
                this.userService.getDataJsonWithParms(this.districtUrl, data)
                    .subscribe(val => { this.getDistrict = val; });
            });

        this.mbDistrict.valueChanges
            .subscribe(data => {
                this.userService.getDataJsonWithParms(this.cantonUrl, this.mbProvince.value, data)
                    .subscribe(val => { this.getCanton = val; });
            });
    }

    // config firstname & lastname.
    public disabledFLName() {
        if (this.facName.value === '') {
            this.fName.enable();
            this.lName.enable();
        } else {
            this.fName.disable();
            this.lName.disable();
        }
    }

    public disabledDFLName() {
        if (this.mbDeptName.value === '') {
            this.mbFName.enable();
            this.mbLName.enable();
        } else {
            this.mbFName.disable();
            this.mbLName.disable();
        }
    }

    public onsubmit(values: Inf.InsertInputs) {

        // Validate operators.
        // for (let propertyName in this.form['controls']) {
        //     if (this.form.controls[propertyName]) {
        //         console.log(propertyName + ' : ' + this.form.controls[propertyName].valid || 'nulll');
        //     }
        // }

        values['operatorId'] = this.operatorId.value.id || this.operatorId.value;
        values['editorialStaffId'] = this.editorialStaffId.value.id || this.editorialStaffId.value;
        values['examinerId'] = this.examinerId.value.id || this.examinerId.value;
        values['recorderStaffId'] = this.recorderStaffId.value.id || this.recorderStaffId.value;

        if (this.getUsers.find(findObjects => findObjects.id === values['operatorId']) !== undefined &&
            this.getUsers.find(findObjects => findObjects.id === values['editorialStaffId']) !== undefined &&
            this.getUsers.find(findObjects => findObjects.id === values['examinerId']) !== undefined &&
            this.getUsers.find(findObjects => findObjects.id === values['recorderStaffId']) !== undefined) {

            values['fName'] = this.fName.value || '';
            values['lName'] = this.lName.value || '';
            values['facName'] = this.facName.value || '';
            delete values['names'];

            if (+this.status.value === 1 && +this.ecoActivity.value !== 0) {
                values['tsicId'] = this.tsicId.value.tsic || this.tsicId.value;
                if (this.getTSIC.find(findObjects => +findObjects.tsic === +values['tsicId']) === undefined) {
                    this.alerts.push({
                        type: 'danger',
                        msg: `กรุณาเลือกรหัสประเภทกิจกรรมทางเศรษฐกิจให้ถูกต้อง`,
                        timeout: 5000
                    });
                } else {
                    // console.log('status = 1');
                    values['saleOwnWeb'] = +this.checkboxModel[0].checked;
                    values['saleOtherWeb'] = +this.checkboxModel[1].checked;
                    values['saleEmarket'] = +this.checkboxModel[2].checked;
                    values['saleSocial'] = +this.checkboxModel[3].checked;
                    values['totalWorkers'] = this.totalWorkers.value || 0;
                    values['workers'] = this.workers.value || 0;
                    values['formatLaw'] = this.formatLaw.value || 0;
                    values['hMaxPrice'] = this.hMaxPrice.value || 0;
                    values['hMinPrice'] = this.hMinPrice.value || 0;
                    values['mbDeptName'] = this.mbDeptName.value || '';
                    values['mbFName'] = this.mbFName.value || '';
                    values['mbLName'] = this.mbLName.value || '';
                    values['otherStatus'] = '';

                    delete values['aboutHotels'];
                    delete values['aboutWorker'];
                    delete values['deptNames'];

                    this.insertOrUpdate(values);
                }
            } else {
                delete values['aboutHotels'];
                delete values['aboutWorker'];
                delete values['deptNames'];
                this.insertOrUpdate(values);
            }

        } else {
            this.alerts = [];
            this.alerts.push({
                type: 'danger',
                msg: `กรุณาเลือกส่วนการอนุมัติให้ถูกต้อง`,
                timeout: 5000
            });
        }
    }

    private insertOrUpdate(values) {
        if (this.censusCode === undefined) {
            // console.log('inserted');
            this.userService.postDataJson(this.insertUrl, values, false).subscribe((res: Response) => {
                // console.log('inserts');
                this.alerts = [];
                if (res.status === 200) {
                    this.alerts.push({
                        type: 'success',
                        msg: `บันทึกข้อมูลสำเร็จ`,
                        timeout: 5000
                    });
                    // clear input fields.
                    this.form.patchValue(new Inf.InsertInputs);
                    this.fName.reset('');
                    this.lName.reset('');
                    this.formatLaw.reset(0);
                    this.totalWorkers.reset(0);
                    this.workers.reset(0);
                    this.hMinPrice.reset(0);
                    this.hMaxPrice.reset(0);

                    this.facName.reset('');
                    this.mbFName.reset('');
                    this.mbLName.reset('');
                    this.mbDeptName.reset('');
                    this.form.markAsUntouched();
                    // this.form.updateValueAndValidity();

                } else {
                    this.alerts.push({
                        type: 'danger',
                        msg: `บันทึกข้อมูลผิดพลาด กรุณาตรวจสอบอีกครั้ง`,
                        timeout: 5000
                    });
                }
            });
        } else {
            // console.log('updated');
            for (const key of Object.keys(values)) {
                if (values[key] === null)
                    values[key] = '';
            }
            values['censusCode'] = this.censusCode;
            this.userService.postDataJson(this.insertUrl, values, false).subscribe((res: Response) => {
                // console.log('update');
                if (res.status === 200) {
                    this.alerts.push({
                        type: 'success',
                        msg: `อัพเดทข้อมูลสำเร็จ`,
                        timeout: 5000
                    });
                    this.routing.navigate(['/project/search']);
                } else {
                    this.alerts.push({
                        type: 'danger',
                        msg: `อัพเดทข้อมูลผิดพลาด กรุณาตรวจสอบอีกครั้ง`,
                        timeout: 5000
                    });
                }
            });
        }
    }

    private clearValidateStatus() {
        this.otherStatus.clearValidators();
        this.otherStatus.updateValueAndValidity();
        this.otherStatus.reset('');
        this.formatLaw.clearValidators();
        this.formatLaw.updateValueAndValidity();
        this.otherFormatLaw.clearValidators();
        this.otherFormatLaw.updateValueAndValidity();
        this.formatEconomy.clearValidators();
        this.formatEconomy.updateValueAndValidity();
        this.otherFormatEconomy.clearValidators();
        this.otherFormatEconomy.updateValueAndValidity();
        this.regisType.clearValidators();
        this.regisType.updateValueAndValidity();
        this.personalId.clearValidators();
        this.personalId.updateValueAndValidity();
        this.nationPartner.clearValidators();
        this.nationPartner.updateValueAndValidity();
        this.aboutWorker.clearValidators();
        this.aboutWorker.updateValueAndValidity();
        this.totalWorkers.clearValidators();
        this.totalWorkers.updateValueAndValidity();
        this.workers.clearValidators();
        this.workers.updateValueAndValidity();
        this.hType.clearValidators();
        this.hType.updateValueAndValidity();
        this.aboutHotels.clearValidators();
        this.aboutHotels.updateValueAndValidity();
        this.deptNames.clearValidators();
        this.deptNames.updateValueAndValidity();
    }

    private createdAbControl() {
        this.road = this.form.controls['road'];
        this.alley = this.form.controls['alley'];
        this.place = this.form.controls['place'];
        this.aNum = this.form.controls['aNum'];
        this.tName = this.form.controls['tName'];
        this.names = <FormGroup>this.form.controls['names'];
        this.fName = this.names.controls['fName'];
        this.lName = this.names.controls['lName'];
        this.facName = this.names.controls['facName'];
        this.status = this.form.controls['status'];
        this.otherStatus = this.form.controls['otherStatus'];
        this.ecoActivity = this.form.controls['ecoActivity'];
        this.typeActivity = this.form.controls['typeActivity'];
        this.formatEconomy = this.form.controls['formatEconomy'];
        this.otherFormatLaw = this.form.controls['otherFormatLaw'];
        this.otherFormatEconomy = this.form.controls['otherFormatEconomy'];
        this.regisType = this.form.controls['regisType'];
        this.personalId = this.form.controls['personalId'];
        this.nationPartner = this.form.controls['nationPartner'];
        this.aboutWorker = <FormGroup>this.form.controls['aboutWorker'];
        this.formatLaw = this.aboutWorker.controls['formatLaw'];
        this.totalWorkers = this.aboutWorker.controls['totalWorkers'];
        this.workers = this.aboutWorker.controls['workers'];
        this.yearActivity = this.form.controls['yearActivity'];
        this.aboutHotels = <FormGroup>this.form.controls['aboutHotels'];
        this.hType = this.form.controls['hType'];
        this.hRoom = this.form.controls['hRoom'];
        this.hMinPrice = this.aboutHotels.controls['hMinPrice'];
        this.hMaxPrice = this.aboutHotels.controls['hMaxPrice'];
        this.hospitalBed = this.form.controls['hospitalBed'];
        this.useComputer = this.form.controls['useComputer'];
        this.useInternet = this.form.controls['useInternet'];
        this.useEService = this.form.controls['useEService'];
        this.tel = this.form.controls['tel'];
        this.web = this.form.controls['web'];
        this.email = this.form.controls['email'];
        this.tsicId = this.form.controls['tsicId'];
        this.mbTitleName = this.form.controls['mbTitleName'];
        this.deptNames = <FormGroup>this.form.controls['deptNames'];
        this.mbFName = this.deptNames.controls['mbFName'];
        this.mbLName = this.deptNames.controls['mbLName'];
        this.mbDeptName = this.deptNames.controls['mbDeptName'];
        this.mbRoad = this.form.controls['mbRoad'];
        this.mbAlley = this.form.controls['mbAlley'];
        this.mbPlace = this.form.controls['mbPlace'];
        this.mbANum = this.form.controls['mbANum'];
        this.mbProvince = this.form.controls['mbProvince'];
        this.mbDistrict = this.form.controls['mbDistrict'];
        this.mbCanton = this.form.controls['mbCanton'];
        this.note = this.form.controls['note'];
        this.operatorId = this.form.controls['operatorId'];
        this.editorialStaffId = this.form.controls['editorialStaffId'];
        this.recorderStaffId = this.form.controls['recorderStaffId'];
        this.examinerId = this.form.controls['examinerId'];

        this.fliter = this.form.controls['fliter'];
    }

    private autocompleListFormatter = (data: any): SafeHtml => {
        let html = `<span>${data.id} : ${data.name}</span>`;
        return this._sanitizer.bypassSecurityTrustHtml(html);
    }

    private autocompleListTSICFormatter = (data: any): SafeHtml => {
        let html = `<span>${data.tsic} : ${data.tsName}</span>`;
        return this._sanitizer.bypassSecurityTrustHtml(html);
    }
}
