export const PAGES_MENU = [
  {
    path: 'project',
    children: [
      // {
      //   path: 'dashboard',
      //   data: {
      //     menu: {
      //       title: 'Dashboard',
      //       icon: 'ion-android-home',
      //       pathMatch: 'partial',
      //       selected: false,
      //       expanded: false,
      //       order: 0
      //     }
      //   }
      // }, {
      //   path: 'inputs',
      //   data: {
      //     menu: {
      //       title: 'inputs',
      //       icon: 'ion-edit',
      //       pathMatch: 'partial',
      //       selected: false,
      //       expanded: false,
      //       order: 100,
      //     }
      //   }
      // }, {
      {
        path: 'search',
        data: {
          menu: {
            title: 'Search',
            icon: 'ion-search',
            pathMatch: 'partial',
            permission: '1',
            selected: false,
            expanded: false,
            order: 100
          }
        }

      }, {
        path: 'report',
        data: {
          menu: {
            title: 'Chart',
            icon: 'ion-stats-bars',
            pathMatch: 'partial',
            permission: '1',
            selected: false,
            expanded: false,
            order: 200
          }
        },
      }, {
        path: 'tables',
        data: {
          menu: {
            title: 'Summary',
            icon: 'ion-navicon-round',
            pathMatch: 'partial',
            permission: '1',
            selected: false,
            expanded: false,
            order: 300
          }
        },
      }, {
        path: 'users',
        data: {
          menu: {
            title: 'User',
            icon: 'ion-android-person',
            pathMatch: 'partial',
            permission: '3',
            selected: false,
            expanded: false,
            order: 400
          }
        }
      }
    ]
  }
];


