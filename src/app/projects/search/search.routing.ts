import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { Search } from './search.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
    {
        path: '',
        component: Search,
        children: [
            //{ path: 'treeview', component: TreeViewComponent }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
