import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder } from '@angular/forms';
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { Router } from '@angular/router';
import { CommonService } from './../../_services/common.service';
import { GetSearch } from './search.interface';
import { FliterComponent } from './../fliter/fliter.component';
import { CVAInputFliter } from './../fliter/fliter.interface';
import { AlertModule } from 'ng2-bootstrap/alert';
import { Response } from '@angular/http';
import 'style-loader!./search.scss';

@Component({
    selector: 'search',
    templateUrl: './search.html',
    styleUrls: ['./../../theme/components/baContentTop/baContentTop.scss']
})

export class Search implements OnInit {

    private fliterSearch: AbstractControl;
    private getFliter = new CVAInputFliter;
    private isCollapsed: boolean = true;
    private alerts: any = [];
    private dataUser: Array<GetSearch> = [];
    private myForm: FormGroup;
    private keyWord: AbstractControl;

    constructor(private fb: FormBuilder, private userService: CommonService, private router: Router) { }

    ngOnInit() {

        // Check Residue Informations.
        this.userService.getDataJson('http://localhost/census_svr/index.php/Users/searchResidueInfo')
            .subscribe(res => {
                if (res.length > 0) {
                    this.alerts.push({
                        type: 'warning',
                        msg: `มีข้อมูลตกค้างมากกว่า 5 ปี ที่ยังไม่ได้รับการอัพเดท`
                    });
                    this.dataUser = res;
                }
            });

        this.myForm = this.fb.group({
            //keyWord: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{13}')])]
            keyWord: [''],
            fliterSearch: [this.getFliter]
        });

        this.keyWord = this.myForm.controls['keyWord'];
        this.fliterSearch = this.myForm.controls['fliterSearch'];
    }

    public onSearch() {
        this.alerts = [];
        this.dataUser = [];
        // console.log(this.myForm.controls['keyWord'].value);
        //  if(this.myForm.valid)
        this.userService.postData1Params('http://localhost/census_svr/index.php/Users/api_search', this.keyWord.value)
            .subscribe(data => {
                //this.myForm.reset();
                // console.log(data)
                if (data === null) {
                    this.alerts.push({
                        type: 'danger',
                        msg: `ไม่พบข้อมูล (เมื่อ ${(new Date()).toLocaleTimeString()})`,
                        timeout: 5000
                    });
                } else {
                    this.dataUser = data;
                }
            });
    }

    public ADBVsearch(values: CVAInputFliter) {
        // console.log(values);
        this.alerts = [];
        this.dataUser = [];
        console.log(values);
        this.userService.postDataJson('http://localhost/census_svr/index.php/Users/api_search_ADBV', values, true)
            .subscribe(data => {
                //console.log(data)
                if (data === null) {
                    this.alerts.push({
                        type: 'danger',
                        msg: `ไม่พบข้อมูล (เมื่อ ${(new Date()).toLocaleTimeString()})`,
                        timeout: 5000
                    });
                } else {
                    this.dataUser = data;
                }
            });
    }
    public OnUpdated(value: any) {
        this.router.navigate(['/project/updates', value]);
    }
    public OnInsert() {
        this.router.navigate(['/project/inputs']);
    }
}
