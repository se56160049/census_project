import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { AlertModule } from 'ng2-bootstrap/alert';
import { FliterModule } from './../fliter/fliter.module';
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { routing } from './search.routing';
import { Search } from './search.component';


@NgModule({
    imports: [
        CommonModule,
        CollapseModule.forRoot(),
        FormsModule,
        NgaModule,
        routing,
        ReactiveFormsModule,
        FliterModule,
        AlertModule.forRoot()
    ],
    declarations: [
        Search
    ]
})

export class SearchModule {

}
