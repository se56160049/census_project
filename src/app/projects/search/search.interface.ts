export class GetSearch {
    public censusId?: number;
    public dateKey?: number;
    public censusCode?: number;
    public comName?: string;
    public aName?: string;
    public cName?: string;
    public dName?: string;
    public pName?: string;
    public rName?: string;
    public actName?: number;

}