export class GetUser {
    public usersId?: string;
    public userName?: string;
    public password?: string;
    public fName?: string;
    public lName?: string;
    public status?: number;
    public prefixId?: string;
    public prefix?: string;
    public trId?: number;
    public trName?: string;
}

export class Get_model {
    public usersId_m?: string;
    public userName_m?: string;
    public password_m?: string;
    public fName_m?: string;
    public lName_m?: string;
    public status_m?: number;
    public prefix_m?: string;
    public trId_m?: number;
    public trName_m?: string;
}

export class Get_prefix {
    public prefixId?: string;
    public prefixName?: string;
}

