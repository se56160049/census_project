import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UsersSevice } from './users.service';
import { GetUser, Get_model, Get_prefix } from './users.interface';
import { EqualPasswordsValidator } from '../../theme/validators';
import { Response } from '@angular/http';
import { CommonService } from './../../_services/common.service';
import { ModalComponent } from 'ng2-bs4-modal/ng2-bs4-modal';
import { AlertModule } from 'ng2-bootstrap/alert';

import 'style-loader!./users.scss';

@Component({
    selector: 'users',
    templateUrl: './users.html',
    styleUrls: ['./../../theme/components/baContentTop/baContentTop.scss'],
    providers: [UsersSevice, EqualPasswordsValidator]
})

export class Users implements OnInit {

    @ViewChild('myModal')
    modal_add: ModalComponent;

    @ViewChild('myModal_del')
    modal_del: ModalComponent;

    @ViewChild('myModal_edit')
    modal_edit: ModalComponent;

    private _url = 'http://localhost/census_svr/index.php/Users/';

    private dataUser: Array<GetUser> = [];
    private dataPrefix: Array<Get_prefix> = [];
    public alerts: any = [];

    private interf: GetUser = new GetUser();
    public form: FormGroup;
    public userName: AbstractControl;
    public prefix: AbstractControl;
    public newPrefix: AbstractControl;
    public fName: AbstractControl;
    public lName: AbstractControl;
    public password: AbstractControl;
    public repeatPassword: AbstractControl;
    public trId: AbstractControl;
    public trName: AbstractControl;
    public passwords: FormGroup;
    public submitted: boolean = false;

    private modal: Get_model = new Get_model();
    public form_edit: FormGroup;
    public userName_m: AbstractControl;
    public prefix_m: AbstractControl;
    public password_m: AbstractControl;
    public fName_m: AbstractControl;
    public lName_m: AbstractControl;
    public status_m: AbstractControl;
    public usersId_m: AbstractControl;
    public trId_m: AbstractControl;
    public trName_m: AbstractControl;

    private addPrefixShow: boolean = false;

    constructor(private service: UsersSevice, private fb: FormBuilder,
        private userervice: CommonService, private zone: NgZone) {

        this.form = fb.group({
            'prefix': [''],
            'newPrefix': ['', Validators.pattern('^[a-zA-Zก-ฮะ-์.]{1,}$')],
            'userName': ['', Validators.pattern('^[0-9]{7,7}$')],
            'fName': ['', Validators.pattern('^[a-zA-Zก-ฮะ-์]{1,}$')],
            'lName': ['', Validators.pattern('^[a-zA-Zก-ฮะ-์]{1,}$')],
            'trId': ['1'],
            'trName': [],
            'passwords': fb.group({
                'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
                'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
            }, { validator: EqualPasswordsValidator.validate('password', 'repeatPassword') })
        });

        this.trId = this.form.controls['trId'];
        this.trName = this.form.controls['trName'];
        this.prefix = this.form.controls['prefix'];
        this.newPrefix = this.form.controls['newPrefix'];
        this.fName = this.form.controls['fName'];
        this.lName = this.form.controls['lName'];
        this.userName = this.form.controls['userName'];
        this.passwords = <FormGroup>this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.repeatPassword = this.passwords.controls['repeatPassword'];

        this.form_edit = fb.group({
            'prefix_m': ['', Validators.compose([Validators.required])],
            'userName_m': ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{7,7}$')])],
            'fName_m': ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Zก-ฮะ-์.]{1,}$')])],
            'lName_m': ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Zก-ฮะ-์.]{1,}$')])],
            'password_m': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            'usersId_m': [],
            'status_m': [],
            'trId_m': [],
            'trName_m': []
        });

        this.trId_m = this.form_edit.controls['trId_m'];
        this.trName_m = this.form_edit.controls['trName_m'];
        this.prefix_m = this.form_edit.controls['prefix_m'];
        this.fName_m = this.form_edit.controls['fName_m'];
        this.lName_m = this.form_edit.controls['lName_m'];
        this.userName_m = this.form_edit.controls['userName_m'];
        this.password_m = this.form_edit.controls['password_m'];
        this.usersId_m = this.form_edit.controls['usersId_m'];
        this.status_m = this.form_edit.controls['status_m'];


    }

    ngOnInit() {
        this.userervice.getDataJson(this._url + 'api_user').subscribe(res => {
            this.dataUser = res;
        });

        this.selectPrefix();

    }

    public resetTable() {
        this.userervice.getDataJson(this._url + 'api_user').subscribe(res => {
            this.dataUser = res;
        });
    }

    public onDel(id: any): void {
        // console.log(id);
        this.service.getdel(id).subscribe(res => { this.dataUser = res; }
        )


    }

    public onShow(id_user: string) {
        //console.log(id_user);
        //console.log(this.dataUser);
        // this.service.getshow(id_user).subscribe(res => { this.dataUser = res; }
        // )
        this.modal_del.open();
        let temp1 = this.dataUser.find(findObjects => findObjects.usersId === id_user)

        // console.log(temp1);
        this.prefix_m.setValue(temp1.prefix)
        this.userName_m.setValue(temp1.userName)
        this.password_m.setValue(temp1.password)
        this.fName_m.setValue(temp1.fName)
        this.lName_m.setValue(temp1.lName)
        this.usersId_m.setValue(temp1.usersId)
    }

    public onShow_edit(id_user: string) {
        //console.log(id_user);
        //console.log(this.dataUser);
        // this.service.getshow(id_user).subscribe(res => { this.dataUser = res; }
        // )
        this.selectPrefix();
        this.modal_edit.open();
        let temp1 = this.dataUser.find(findObjects => findObjects.usersId === id_user)

        // console.log(temp1);
        this.prefix_m.setValue(temp1.prefixId)
        this.userName_m.setValue(temp1.userName)
        this.password_m.setValue(temp1.password)
        this.fName_m.setValue(temp1.fName)
        this.lName_m.setValue(temp1.lName)
        this.usersId_m.setValue(temp1.usersId)
        this.status_m.setValue(temp1.status)
        this.trId_m.setValue(temp1.trId)
    }

    public onSubmit(values: any): void {

        this.submitted = true;
        if (this.form.valid) {
            //check new prefix
            if(this.addPrefixShow === false ){
                //old
                this.interf.prefix = values.prefix;
            }else{
                //new
                let temp = this.dataPrefix.find(findObjects => findObjects.prefixName === values.newPrefix);
                //check have prefix
                if (temp === undefined) {
                    //not have prefix
                    this.interf.prefix = values.newPrefix;
                }else{
                    //have prefix
                    this.alerts.push({
                            type: 'danger',
                            msg: `มีคำนำหน้าชื่ออยู่แล้ว`,
                            timeout: 5000
                        });
                    return;
                }
                
            }

            //this.interf.prefix = values.prefix;
            console.log(this.interf.prefix);
            this.interf.userName = values.userName;
            this.interf.password = values.passwords.password;
            this.interf.fName = values.fName;
            this.interf.lName = values.lName;
            this.interf.trId = values.trId;
            // this.service.post_regis(this.interf)
            //     .subscribe((res:Response) => console.log(res.status));
            this.service.post_regis(this.interf)
                .subscribe((res: Response) => {
                    // console.log(res.status)
                    if (res.status === 204) {
                        this.alerts.push({
                            type: 'danger',
                            msg: `Username ซ้ำ กรุณาเพิ่มข้อมูลใหม่ (เมื่อ ${(new Date()).toLocaleTimeString()})`,
                            timeout: 5000
                        });
                    } else {
                        this.alerts.push({
                            type: 'info',
                            msg: `บันทึกข้อมูลสำเร็จ (เมื่อ ${(new Date()).toLocaleTimeString()})`,
                            timeout: 5000
                        });
                    }
                    this.zone.run(() => {
                        this.resetTable();
                    });
                    // this.form.patchValue(new GetUser);
                    this.addPrefixShow = false;
                    this.prefix.reset('');
                    this.newPrefix.reset();
                    this.fName.reset();
                    this.lName.reset();
                    this.userName.reset();
                    this.password.reset();
                    this.repeatPassword.reset();
                    this.trId.reset('1');
                    
                });

        }
    }

    public onUpdate(data: any): void {


        this.modal.prefix_m = data.prefix_m;
        this.modal.userName_m = data.userName_m;
        this.modal.password_m = data.password_m;
        this.modal.fName_m = data.fName_m;
        this.modal.lName_m = data.lName_m;
        this.modal.usersId_m = data.usersId_m;
        this.modal.status_m = data.status_m;
        this.modal.trId_m = data.trId_m;
        this.service.post_update(this.modal)
            .subscribe((res: Response) => {
                // console.log(res.status)
                if (res.status === 204) {
                    this.alerts.push({
                        type: 'danger',
                        msg: `Username ซ้ำ กรุณาแก้ไขข้อมูลใหม่ (เมื่อ ${(new Date()).toLocaleTimeString()})`,
                        timeout: 5000
                    });
                } else {
                    this.alerts.push({
                        type: 'info',
                        msg: `แก้ไขข้อมูลสำเร็จ (เมื่อ ${(new Date()).toLocaleTimeString()})`,
                        timeout: 5000
                    });
                }
                this.zone.run(() => {
                    this.resetTable();
                });
            });
 }

    close() {
        this.modal_add.close();
        this.modal_del.close();
        this.modal_edit.close();
    }

    open() {
        this.modal_add.open();
        this.selectPrefix();

    }

    public selectPrefix() {
        this.service.get_prefix()
            .subscribe(res => {
                this.dataPrefix = res;

            });

    }
}