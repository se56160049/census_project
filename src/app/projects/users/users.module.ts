import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { AlertModule } from 'ng2-bootstrap/alert';
import { routing } from './users.routing';
import { Users } from './users.component';
import { ModalModule } from 'ng2-bs4-modal/ng2-bs4-modal';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgaModule,
        routing,
        ReactiveFormsModule,
        ModalModule,
        AlertModule.forRoot()
    ],
    declarations: [
        Users
    ]
})

export class UsersModule { }
