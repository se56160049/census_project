import { Injectable } from '@angular/core'
import { GetUser, Get_model } from './users.interface'
import { Http, Headers, RequestOptions } from '@angular/http'

@Injectable()
export class UsersSevice {

    private _url = 'http://localhost/census_svr/index.php/Users/'
    private headers: any;
    private options: any;
    // body = `username=${}&password=${p}&fname=${},&lname=${}`;

    constructor(private http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

        this.options = new RequestOptions({ headers: this.headers, withCredentials: false });
    }

    public getuser() {
        return this.http.get(this._url + 'api_user').map(res => res.json())

    }

    public getdel(id) {
        return this.http.get(this._url + 'user_del/' + id).map(res => res.json())
    }
    // public getSearch(keyword:any){
    //     return this.http.get(this._url + 'api_search/' + keyword).map(res => res.json())
    // }

    public getshow(idUser) {
        return this.http.get(this._url + 'api_user_show' + idUser).map(res => res.json())

    }

    public post_regis(regis: GetUser) {

        let body = `username=${regis.userName}&prefix=${regis.prefix}&password=${regis.password}&fname=${regis.fName}&lname=${regis.lName}&trId=${regis.trId}`;

        return this.http.post(this._url + 'add_user', body, this.options)

    }

    public post_update(update: Get_model) {

        let body = `username=${update.userName_m}&prefix=${update.prefix_m}&password=${update.password_m}&fname=${update.fName_m}&lname=${update.lName_m}&userid=${update.usersId_m}&status=${update.status_m}&trId=${update.trId_m}`;

        return this.http.post(this._url + 'edit_user', body, this.options)
     
    }

    public get_typeuser() {
        return this.http.get(this._url + 'api_typeUser').map(res => res.json())

    }

     public get_prefix() {
        return this.http.get(this._url + 'api_prefixs').map(res => res.json())

    }
}
