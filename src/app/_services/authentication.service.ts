import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {

    private headers: any;
    private options: any;

    constructor(private http: Http, private router: Router) {

        // setTimeout(() => {
        //     localStorage.removeItem('username');
        //     localStorage.removeItem('id_token');
        // }, 14400);

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.options = new RequestOptions({ headers: this.headers, withCredentials: false });
    }

    public login(username: string, password: string) {

        let body = `username=${username}&password=${password}`;

        return this.http.post('http://localhost/census_svr/index.php/auth/login',
            body, this.options)
            .map((response: Response) => {
                if (response.status === 200) {
                    localStorage.setItem('username', username);
                    localStorage.setItem('id_token', response.json().token);
                    localStorage.setItem('permission', response.json().permission);
                    return true;
                } else {
                    return false;
                }
            });
    }

    public logout() {
        // clear token remove user from local storage to log user out
        localStorage.removeItem('username');
        localStorage.removeItem('id_token');
        localStorage.removeItem('permission');
        this.router.navigate(['/login']);
    }

}
