import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AuthenticationService } from './authentication.service';

@Injectable()
export class CommonService {

    private headers: any;
    private options: any;
    private username: any;

    constructor(private http: Http, private auth: AuthenticationService) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.options = new RequestOptions({ headers: this.headers, withCredentials: false });

    }

    public getDataJson(url: string) {
        return this.http.get(url, this.options)
            .map(res => res.json());
    }

    public postData1Params(url: string, data: any) {

        let body = `data=${data}`;
        return this.http.post(url, body, this.options)
            .map(res => res.json());
    }

     public postData2Params(url: string, data: any) {

        let body = `data=${data}`;
        return this.http.post(url, body, this.options);
    }


    public getDataJsonWithParms(url: string, ...arg) {

        if (arg != null) {
            let parms: string = '';

            arg.forEach(element => {
                parms += '/' + element;
            });
            return this.http.get(url + parms, this.options)
                .map(res => res.json());
        }
    }

    public postDataJson(url: string, databody: any, returned: boolean = true): any {

        let body: string = '';

        // genarate body (can use nested 2 objects).
        for (const key of Object.keys(databody)) {
            if (typeof databody[key] === 'object') {
                let temp = databody[key];
                for (const keys of Object.keys(temp)) {
                    // typeof temp[keys] === 'string' ?
                    //     body += `${keys}='${temp[keys]}'&&` :
                    body += `${keys}=${temp[keys]}&&`;
                }
            } else {
                // typeof databody[key] === 'string' ?
                //     body += `${key}='${databody[key]}'&&` :
                body += `${key}=${databody[key]}&&`;
            }
        }

        // cut && in last characters.
        body = body.substring(0, body.length - 2);

        // console.log(body);
        if (returned) {
            return this.http.post(url, body, this.options).map(res => res.json());
        } else {
            return this.http.post(url, body, this.options);
        }
    }

}
