import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
  { path: '', redirectTo: 'project/search', pathMatch: 'full' },
  { path: '**', redirectTo: 'project/search' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: false });
