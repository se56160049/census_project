import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {
    private permission: number;
    constructor(private router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
         this.permission = +localStorage.getItem('permission');
        if (this.permission > 2) {
            // logged in so return true
            return true;
        } else {
            // not logged in so redirect to login page
            //this.router.navigate(['/project/search']);
            return false;
        }
    }
}
